import 'package:flutter/material.dart';
import 'package:tictactoe/SizeConfig.dart';
import 'package:tictactoe/user_simple_preferences.dart';
Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await UserSimplePreferences.init();
  runApp(
    const MyApp()
    );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.red,
        scaffoldBackgroundColor: const Color.fromARGB(255, 0, 0, 0),
      ),
      home: const MyHomePage(title: 'TIC TAC TOE'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

var res=List.generate(3, (index) => List.generate(3,(index)=> -1));
int _playerTurn = 1;
int _player1Wins=0,_player2Wins=0;
String score="0:0";
bool winnerFound=false;
int _gameCount=0;

class _MyHomePageState extends State<MyHomePage> {
  void finishGame(){
    _gameCount++;
    UserSimplePreferences.setGameCount(_gameCount);
    _player1Wins=_player2Wins=0;
    _reset();
  }
  void _popup() {
    showDialog(
    context: context,
    builder: (BuildContext context) => _buildPopupDialog(context)
    ).then((val){
      _reset();
    });
  }
  void _drawpopup(){
    showDialog(
      context: context,
      builder: (BuildContext context) => _buildPopupDialogDraw(context)
    ).then((val){
      _reset();
    });
  }
  void _reset()
  {
    setState((){
      for(var i=0;i<3;i++)
      {
        for(var j=0;j<3;j++)
        {
          res[i][j]=-1;
          UserSimplePreferences.setGrid(i,j,-1);
        }
      }
    _playerTurn=1;
    UserSimplePreferences.setPlayerTurn(_playerTurn);
    score=_player1Wins.toString();
    score="$score:";
    String temp=_player2Wins.toString();
    score="$score$temp";
    });
  }
  void _Player1Wins(){
    _player1Wins++;
    UserSimplePreferences.setPlayer1Score(_gameCount,_player1Wins);
    _popup();
  }
  void _Player2Wins(){
    _player2Wins++;
    UserSimplePreferences.setPlayer2Score(_gameCount,_player2Wins);
    _popup();
  }
  void _onTap(int i,int j){
    setState(() {
      if(res[i][j]==-1)
      {
        if(_playerTurn==1)
        {
          res[i][j]=0;
          UserSimplePreferences.setGrid(i,j,0);
        }
        else
        {
          res[i][j]=1;
          UserSimplePreferences.setGrid(i,j,1);
        }
        _findWinner();
      }
    });
  }
  void _findWinner()
    {
      int rowSum=0,colSum=0,leftdiagSum=0,rightdiagSum=0;
      for(var i=0;i<3;i++)
      {
        for(var j=0;j<3;j++)
        {
          rowSum+= (res[i][j]==-1)? -3:res[i][j];
          colSum+= (res[j][i]==-1)? -3:res[j][i];
        }
        leftdiagSum+=(res[i][i]==-1)? -3: res[i][i];
        rightdiagSum+=(res[i][2-i]==-1)? -3:res[i][2-i];
        if(rowSum==0||colSum==0||(i==2&&leftdiagSum==0)||(i==2&&rightdiagSum==0))
        {
          _Player1Wins();
          return;
        }
        if(rowSum==3||colSum==3||(i==2&&leftdiagSum==3)||(i==2&&rightdiagSum==3))
        {
          _Player2Wins();
          return;
        }
        rowSum=colSum=0;
      }
      int emptyCells=0;
      for(var i=0;i<3;i++)
      {
        for(var j=0;j<3;j++)
        {
          emptyCells+= (res[i][j]==-1)? 1:0;
        }
      }
      if(emptyCells==0)
      {
        _drawpopup();
        return;
      }
      _playerTurn=(_playerTurn==1)? 2:1;
      UserSimplePreferences.setPlayerTurn(_playerTurn);
    }
  
  @override
  void initState(){
    super.initState();

    _playerTurn=UserSimplePreferences.getPlayerTurn() ?? 1;
    UserSimplePreferences.setPlayerTurn(_playerTurn);
    _gameCount=UserSimplePreferences.getGameCount() ?? 1;
    if(_gameCount==1)
    {
      UserSimplePreferences.setGameCount(_gameCount);
    }
    _player1Wins=UserSimplePreferences.getPlayer1Score(_gameCount) ?? 0;
    _player2Wins=UserSimplePreferences.getPlayer2Score(_gameCount) ?? 0;
    score=_player1Wins.toString();
    score="$score:";
    String temp=_player2Wins.toString();
    score="$score$temp";
    for(var i=0;i<3;i++)
    {
      for(var j=0;j<3;j++)
      {
        res[i][j]=UserSimplePreferences.getGrid(i,j) ?? -1;
        if(res[i][j]==-1)
        {
          UserSimplePreferences.setGrid(i,j,res[i][j]);
        }
      }
    }
  }
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    String _name=_playerTurn.toString();
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children:<Widget>[
              Container(
                // color: Colors.red,
                margin: const EdgeInsets.only(right:0,top: 0,left: 0,bottom: 0),
                child: IconButton(
                  icon: Image.asset('assets/images/leftarrow.png'),
                  iconSize: SizeConfig.blockSizeHorizontal!*9,
                  onPressed: (){
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context)=> const HomeRoute()),
                    );
                  }, 
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children:<Widget>[
                  Padding(
                    padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal!*2),
                    child: Image.asset('assets/images/cross.png',height:SizeConfig.blockSizeVertical!*13,width:SizeConfig.blockSizeVertical!*10,),
                  ),
                  (_playerTurn==1)? Text("Player 1",style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal!*6,
                  color: Colors.transparent,
                  shadows: [Shadow(offset: Offset(0,-(SizeConfig.blockSizeHorizontal!*3)), color: Colors.white)],decoration: TextDecoration.underline,decorationThickness: SizeConfig.blockSizeHorizontal!*0.5,decorationColor: Colors.red),):
                  Text("Player 1",style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal!*6,
                  color: Colors.transparent,
                  shadows: [Shadow(offset: Offset(0,-(SizeConfig.blockSizeHorizontal!*3)), color: Colors.white)],),),
                ],
              ),
              Container(
                alignment: Alignment.center,
                height: SizeConfig.blockSizeVertical!*11,
                width: SizeConfig.blockSizeVertical!*10,
                child: Text(score,style: TextStyle(fontSize: (SizeConfig.blockSizeHorizontal!>SizeConfig.blockSizeVertical!) ? SizeConfig.blockSizeHorizontal!*3.5:SizeConfig.blockSizeVertical!*3.5,color: Colors.white),),
              ),
              Column(
                children:<Widget>[
                  Padding(
                    padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal!*2),
                    child: Image.asset('assets/images/zero.png',height:SizeConfig.blockSizeVertical!*13,width:SizeConfig.blockSizeVertical!*10,),
                  ),
                  (_playerTurn==2)? Text("Player 2",style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal!*6,
                  // color: Colors.white,
                  color: Colors.transparent,
                  shadows: [Shadow(offset: Offset(0,-(SizeConfig.blockSizeHorizontal!*3)), color: Colors.white)],decoration: TextDecoration.underline,decorationThickness: SizeConfig.blockSizeHorizontal!*0.5,decorationColor: Colors.red),):
                  Text("Player 2",style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal!*6,
                  color: Colors.transparent,
                  shadows: [Shadow(offset: Offset(0,-(SizeConfig.blockSizeHorizontal!*3)), color: Colors.white)],),),
                ],
              ),
              Container(
                // color: Colors.red,
                margin: const EdgeInsets.only(right:0,top: 0,left: 0,bottom: 0),
                child: IconButton(
                  icon: Image.asset('assets/images/settings.jpeg'),
                  iconSize: SizeConfig.blockSizeHorizontal!*9,
                  onPressed: (){
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context)=> const SettingRoute()),
                    );
                  }, 
                ),
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.fromLTRB(0,SizeConfig.blockSizeVertical!*10,0,0),
            padding: const EdgeInsets.all(0),
            child: Table(
              defaultVerticalAlignment: TableCellVerticalAlignment.middle,
              columnWidths: {
                0:FixedColumnWidth(SizeConfig.blockSizeVertical!*15.0),
                1:FixedColumnWidth(SizeConfig.blockSizeVertical!*15.0),
                2:FixedColumnWidth(SizeConfig.blockSizeVertical!*15.0)
              },
              border: TableBorder.all(color: Colors.white,width: 0.5),
              children: [
                TableRow(
                  children: [
                    Material(
                      color: Colors.black,
                      child: InkWell(
                        onTap:(){
                          _onTap(0, 0);
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(6.0),
                          child: (res[0][0]==0)? Image.asset("assets/images/cross.png",height:SizeConfig.blockSizeVertical!*13,width:SizeConfig.blockSizeVertical!*11,):
                          ((res[0][0]==1)? Image.asset("assets/images/zero.png",height:SizeConfig.blockSizeVertical!*13,width:SizeConfig.blockSizeVertical!*11,): 
                          Image.asset("assets/images/empty.png",height:SizeConfig.blockSizeVertical!*13,width:SizeConfig.blockSizeVertical!*11,))
                          ,
                        ),
                      ),
                    ),
                    Material(
                      color: Colors.black,
                      child: InkWell(
                        onTap:(){
                          _onTap(0, 1);
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(6.0),
                          child: (res[0][1]==0)? Image.asset("assets/images/cross.png",height:SizeConfig.blockSizeVertical!*13,width:SizeConfig.blockSizeVertical!*11,):
                          ((res[0][1]==1)? Image.asset("assets/images/zero.png",height:SizeConfig.blockSizeVertical!*13,width:SizeConfig.blockSizeVertical!*11,): 
                          Image.asset("assets/images/empty.png",height:SizeConfig.blockSizeVertical!*13,width:SizeConfig.blockSizeVertical!*11,))
                          ,
                        ),
                      ),
                    ),
                    Material(
                      color: Colors.black,
                      child: InkWell(
                        onTap:(){
                          _onTap(0, 2);
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(6.0),
                          child: (res[0][2]==0)? Image.asset("assets/images/cross.png",height:SizeConfig.blockSizeVertical!*13,width:SizeConfig.blockSizeVertical!*11,):
                          ((res[0][2]==1)? Image.asset("assets/images/zero.png",height:SizeConfig.blockSizeVertical!*13,width:SizeConfig.blockSizeVertical!*11,): 
                          Image.asset("assets/images/empty.png",height:SizeConfig.blockSizeVertical!*13,width:SizeConfig.blockSizeVertical!*11,))
                          ,
                        ),
                      ),
                    ),
                  ]
                ),
                TableRow(
                  children: [
                    Material(
                      color: Colors.black,
                      child: InkWell(
                        onTap:(){
                          _onTap(1, 0);
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(6.0),
                          child: (res[1][0]==0)? Image.asset("assets/images/cross.png",height:SizeConfig.blockSizeVertical!*13,width:SizeConfig.blockSizeVertical!*11,):
                          ((res[1][0]==1)? Image.asset("assets/images/zero.png",height:SizeConfig.blockSizeVertical!*13,width:SizeConfig.blockSizeVertical!*11,): 
                          Image.asset("assets/images/empty.png",height:SizeConfig.blockSizeVertical!*13,width:SizeConfig.blockSizeVertical!*11,))
                          ,
                        ),
                      ),
                    ),
                    Material(
                      color: Colors.black,
                      child: InkWell(
                        onTap:(){
                          _onTap(1, 1);
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(6.0),
                          child: (res[1][1]==0)? Image.asset("assets/images/cross.png",height:SizeConfig.blockSizeVertical!*13,width:SizeConfig.blockSizeVertical!*11,):
                          ((res[1][1]==1)? Image.asset("assets/images/zero.png",height:SizeConfig.blockSizeVertical!*13,width:SizeConfig.blockSizeVertical!*11,): 
                          Image.asset("assets/images/empty.png",height:SizeConfig.blockSizeVertical!*13,width:SizeConfig.blockSizeVertical!*11,))
                          ,
                        ),
                      ),
                    ),
                    Material(
                      color: Colors.black,
                      child: InkWell(
                        onTap:(){
                          _onTap(1, 2);
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(6.0),
                          child: (res[1][2]==0)? Image.asset("assets/images/cross.png",height:SizeConfig.blockSizeVertical!*13,width:SizeConfig.blockSizeVertical!*11,):
                          ((res[1][2]==1)? Image.asset("assets/images/zero.png",height:SizeConfig.blockSizeVertical!*13,width:SizeConfig.blockSizeVertical!*11,): 
                          Image.asset("assets/images/empty.png",height:SizeConfig.blockSizeVertical!*13,width:SizeConfig.blockSizeVertical!*11,))
                          ,
                        ),
                      ),
                    ),
                  ]
                ),
                TableRow(
                  children: [
                    Material(
                      color: Colors.black,
                      child: InkWell(
                        onTap:(){
                          _onTap(2,0);
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(6.0),
                          child: (res[2][0]==0)? Image.asset("assets/images/cross.png",height:SizeConfig.blockSizeVertical!*13,width:SizeConfig.blockSizeVertical!*11,):
                          ((res[2][0]==1)? Image.asset("assets/images/zero.png",height:SizeConfig.blockSizeVertical!*13,width:SizeConfig.blockSizeVertical!*11,): 
                          Image.asset("assets/images/empty.png",height:SizeConfig.blockSizeVertical!*13,width:SizeConfig.blockSizeVertical!*11,))
                          ,
                        ),
                      ),
                    ),
                    Material(
                      color: Colors.black,
                      child: InkWell(
                        onTap:(){
                          _onTap(2,1);
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(6.0),
                          child: (res[2][1]==0)? Image.asset("assets/images/cross.png",height:SizeConfig.blockSizeVertical!*13,width:SizeConfig.blockSizeVertical!*11,):
                          ((res[2][1]==1)? Image.asset("assets/images/zero.png",height:SizeConfig.blockSizeVertical!*13,width:SizeConfig.blockSizeVertical!*11,): 
                          Image.asset("assets/images/empty.png",height:SizeConfig.blockSizeVertical!*13,width:SizeConfig.blockSizeVertical!*11,))
                          ,
                        ),
                      ),
                    ),
                    Material(
                      color: Colors.black,
                      child: InkWell(
                        onTap:(){
                          _onTap(2, 2);
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(6.0),
                          child: (res[2][2]==0)? Image.asset("assets/images/cross.png",height:SizeConfig.blockSizeVertical!*13,width:SizeConfig.blockSizeVertical!*11,):
                          ((res[2][2]==1)? Image.asset("assets/images/zero.png",height:SizeConfig.blockSizeVertical!*13,width:SizeConfig.blockSizeVertical!*11,): 
                          Image.asset("assets/images/empty.png",height:SizeConfig.blockSizeVertical!*13,width:SizeConfig.blockSizeVertical!*11,))
                          ,
                        ),
                      ),
                    ),
                  ]
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(0,SizeConfig.blockSizeHorizontal!*5,0,0),
            child: Center(
              child: ElevatedButton(
                onPressed: (){
                  finishGame();
                },
                child: const Text("Finish Game"),
              ),
            ),
          )
        ],
      ),
    );
  }
}

class HomeRoute extends StatelessWidget{
  const HomeRoute({super.key});

  @override
  Widget build(BuildContext context)
  {
    return Scaffold(
      appBar: AppBar(
        title: const Text('WELCOME TO GAME'),
        automaticallyImplyLeading: false,
      ),
      body: Center(
        child: Column(
          // mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text("ScoreBoard",style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal!*10,color: Colors.white),),
            DataTable(
              columns: [
              DataColumn(label: Text('Game No.',style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal!*4.5,color: Colors.white),)),
              DataColumn(label: Text('Player 1',style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal!*4.5,color: Colors.white),)),
              DataColumn(label: Text('Player 2',style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal!*4.5,color: Colors.white),)),
              ],
              rows: List<DataRow>.generate(_gameCount-1,(int index)=>
                  DataRow(
                    cells: <DataCell>[
                      DataCell(Text((index+1).toString(),style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal!*5,color: Colors.white),)),
                      DataCell(Text((UserSimplePreferences.getPlayer1Score(index+1)?? 0).toString(),style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal!*5,color: Colors.white),)),
                      DataCell(Text((UserSimplePreferences.getPlayer2Score(index+1)?? 0).toString(),style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal!*5,color: Colors.white),)),
                    ],
                  ),
              ),
            ),
            ElevatedButton(
              onPressed: (){
                Navigator.pop(context);
              },
              child: const Text("Play Game"),
            ),
          ]
        ),
      ),
    );
  }
}

class SettingRoute extends StatelessWidget{
  const SettingRoute({super.key});

  @override
  Widget build(BuildContext context)
  {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Settings'),
        automaticallyImplyLeading: false,
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: (){
            Navigator.pop(context);
          },
          child: const Text("Back to Game"),
        ),
      ),
    );
  }
}

Widget _buildPopupDialog(BuildContext context) {
  String playerWin=_playerTurn.toString();
  return AlertDialog(
    title: const Text('Congratulations!!!'),
    content: Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text("Player $playerWin wins"),
      ],
    ),
    actions: <Widget>[
      TextButton(
        onPressed: () {
          Navigator.of(context).pop();
        },
        child: const Text('Close'),
      ),
    ],
  );
}
Widget _buildPopupDialogDraw(BuildContext context) {
  return AlertDialog(
    title: const Text('Game Draws..'),
    content: Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: const <Widget>[
        Text("Play one more time"),
      ],
    ),
    actions: <Widget>[
      TextButton(
        onPressed: () {
          Navigator.of(context).pop();
        },
        child: const Text('Close'),
      ),
    ],
  );
}

