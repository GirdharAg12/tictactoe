import 'package:shared_preferences/shared_preferences.dart';

class UserSimplePreferences{
  static late SharedPreferences _preferences;

  static Future init() async =>
    _preferences= await SharedPreferences.getInstance();
    
  static Future setPlayer1Score(int gameCount,int player1Wins) async{
    await _preferences.setInt("$gameCount p1", player1Wins);
  }

  static Future setPlayer2Score(int gameCount,int player2Wins) async{
    await _preferences.setInt("$gameCount p2", player2Wins);
  }

  static Future setPlayerTurn(int playerTurn) async{
    await _preferences.setInt("playerTurn", playerTurn);
  }

  static Future setGrid(int i,int j,int val) async{
    await _preferences.setInt("G$i$j",val);
  }
  
  static Future setGameCount(int count) async{
    await _preferences.setInt("gameCount", count);
  }

  
  static int? getPlayer1Score(int gamecount) => _preferences.getInt("$gamecount p1");
  static int? getPlayer2Score(int gamecount) => _preferences.getInt("$gamecount p2");
  static int? getGrid(int i,int j) => _preferences.getInt("G$i$j");
  static int? getGameCount() => _preferences.getInt("gameCount");
  static int? getPlayerTurn() => _preferences.getInt("playerTurn");
}